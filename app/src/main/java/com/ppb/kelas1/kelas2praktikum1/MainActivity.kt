package com.ppb.kelas1.kelas2praktikum1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.ppb.kelas1.kelas2praktikum1.model.Student

class MainActivity : AppCompatActivity() {
    //variabel global
    lateinit var btnPindah: Button
    lateinit var btnPindahDenganData: Button
    lateinit var btnPindahDenganObjek: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //inisialisasi variabel
        btnPindah = findViewById(R.id.btn_pindah)
        btnPindahDenganData = findViewById(R.id.btn_pindah_dengan_data)
        btnPindahDenganObjek = findViewById(R.id.btn_pindah_dengan_objek)

        actionButtonPindah()
        actionButtonPindahDenganData()
        actionButtonPindahDenganObjek()
    }

    fun actionButtonPindahDenganObjek() {
        val student = Student(
            "2342101234",
            "Gibran Ponakan Paman",
            34,
            true
        )
        btnPindahDenganObjek.setOnClickListener {
            val intent = Intent(this, PindahDenganObjekActivity::class.java)
            intent.putExtra("student", student)
            startActivity(intent)
        }
    }

    fun actionButtonPindahDenganData() {
        btnPindahDenganData.setOnClickListener {
            val intent = Intent(this, PindahDenganDataActivity::class.java)
            intent.putExtra("nama", "Seiko")
            intent.putExtra("umur", 25)
            startActivity(intent)
        }
    }

    fun actionButtonPindah() {
        btnPindah.setOnClickListener {
            val intent = Intent(this, PindahActivity::class.java)
            startActivity(intent)
        }
    }

}